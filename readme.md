#Explorer
Robot simulator exploring planet Mars

##Instructions
  run application from root directory using 
 
    ./mission_mars.rb

  expected output:

    "1 1 E"
    "3 3 N LOST"
    "2 3 S"

  run tests from root directory using  

    ruby explorer_test.rb


## Future work:

- Improve unit test coverage - especially stepping has no coverage, tested only via integration tested.
- Supply benchmark

## Summary

- Total time spent 3-5 hours