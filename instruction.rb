class Instruction
  def initialize(instruction)
    @instruction = instruction
  end

  def action
    if rotate?
      return 'rotate_' + humanized_rotate_instructions
    else
      'step'
    end
  end

  def rotate?
    rotate_instructions.include?(@instruction)
  end

  def humanized_rotate_instructions
    if @instruction == 'L'
      return 'left'
    elsif @instruction == 'R'
      return 'right'
    else
      raise "#{@instruction} is not rotate instruction"
    end
  end

  def rotate_instructions
    %(L R)
  end
end