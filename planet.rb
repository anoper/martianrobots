class Planet
  attr_reader :name, :boundaries
  def initialize(boundaries, name)
    @boundaries = boundaries
    @name = name
    @rescued_coordinations = []
  end

  def on_planet? x_axis, y_axis
    within_x_boundary = (boundaries.first >= x_axis && x_axis >= 0)
    within_y_boundary = (boundaries.last >= y_axis && y_axis >= 0)

    (within_x_boundary && within_y_boundary)
  end

  def rescued_object? position
    @rescued_coordinations.include?(position)
  end

  def set_rescue_object_on position
    @rescued_coordinations << position
  end
end