require_relative 'robot_motions'
require_relative 'instruction'

class Robot
  include RobotMotions
  attr_reader :current_position, :current_orientation, :live
  def initialize(instructions, planet)
    @planet = planet

    @current_position = set_on_planet(instructions[:start_instruction])
    @current_orientation = instructions[:start_instruction].orientation
    @instructions = load_instructions(instructions[:instructions])
    @alive = true
  end

  def go
    @instructions.each do |instruction|
      if alive?
        send(instruction.action)
      else
        break # Robot felt over the clif and can not listen to instructions anymore
      end
    end
  end

  def set_on_planet start_instruction
    current_position = start_instruction.coordinates
  end

  def validate_liveness
    die unless @planet.on_planet?(@current_position.first, @current_position.last)
  end


  def print_report_state
    p self.to_s
  end

  def to_s
    "#{current_position.join(" ")} #{current_orientation}"
  end
  
  private
  def load_instructions(instructions_str)
    instructions = []
    instructions_str.split('').each do |instruction|
      instructions << Instruction.new(instruction)
    end

    instructions
  end

  def alive?
    @alive
  end

  def die
    @alive = false
    @current_orientation = "#{@current_orientation} LOST"
  end
end