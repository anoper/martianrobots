require 'test/unit'
require_relative 'explorer'

TEST_FILE_PATH = 'test_instructions.txt'

module Utils
  def load_test_instructions(path)
    File.read(path).lines
  end
end

class TestExplorer < Explorer
  def initialize; end
end

class TestProcessInstructions < Test::Unit::TestCase
  include Utils

  def setup
    @explorer = TestExplorer.new
  end

  def teardown
    @explorer = nil
  end

  # top level integration test
  def test_load_valid_planet_boundaries
    raw_instructions = load_test_instructions(TEST_FILE_PATH)
    all_instructions = @explorer.process_instructions(raw_instructions)

    assert((all_instructions[:planet_boundaries] == [5,3]), 'array of two integers')
    # @explorer.explore(all_instructions[:planet_boundaries], all_instructions[:go_instructions])
  end

  # Extract to separate test case class
  def returns_go_instructions_has_go_instructions
    raw_instructions = ["1 1 E\n", "RFRFRFRF\n"]
    all_instructions = @explorer.process_instructions(raw_instructions)

    assert(all_instructions(:go_instructions))
  end

  def test_raise_exception_for_invalid_instruction
    # 'X' is not valid instruction
    raw_instructions = ["1 1 E\n", "RFXFRFRF\n"]
    assert_raise ArgumentError do
      assert(@explorer.process_robot_instructions(raw_instructions))
    end
  end

  def test_raise_exception_for_instructions_too_long
    # 'X' is not valid instruction
    too_long_instructions =  101.times.map{'F'}.join
    raw_instructions = ["1 1 E\n", too_long_instructions]

    assert_raise ArgumentError do
      assert(@explorer.process_robot_instructions(raw_instructions), 'Instruction set can not be longer then 100 instrucitons')
    end
  end

  # unit test for coordinates
  def test_raise_exception_for_invalid_coordiantes_lenght
    assert_raise ArgumentError do
      @explorer.parse_coordinates(["1", "2", "3"])
    end
  end

  def test_raise_exception_for_invalid_coordiantes_format
    assert_raise ArgumentError do
      @explorer.parse_coordinates(["a", "3"])
    end
  end

  def test_raise_exception_if_coordinate_x_out_of_range
    assert_raise ArgumentError do
      assert(@explorer.parse_coordinates(["51", "3"]) == [5, 3])
    end
  end

  def test_raise_exception_if_coordinate_y_out_of_range
    assert_raise ArgumentError do
      assert(@explorer.parse_coordinates(["49", "51"]) == [5, 3])
    end
  end

  def test_returns_coordinates
    assert(@explorer.parse_coordinates(["0", "49"]) == [0, 49])
  end
end

class TestParseGoInstructions < Test::Unit::TestCase
  include Utils

  def setup
    @explorer = TestExplorer.new
  end

  def teardown
    @explorer = nil
  end

  def test_returns_valid_start_directive_coordinates
    raw_instruction = ["1 1 E\n", "RFRFRFRF\n"]
    start_coordinates = @explorer.process_go_instruction(raw_instruction)[:start_instruction].coordinates
    assert(start_coordinates == [1, 1])
  end


  def test_returns_valid_orientation
    raw_instruction = ["1 1 E\n", "RFRFRFRF\n"]
    orientation = @explorer.process_go_instruction(raw_instruction)[:start_instruction].orientation
    assert(orientation == "E")
  end

  def test_raises_exception_when_invalid_orientation
    raw_instruction = ["1 1 X\n", "RFRFRFRF\n"]
    assert_raise ArgumentError do
      @explorer.process_go_instruction(raw_instruction)[:start_instruction].orientation
    end
  end
end

# Integration test
class TestGo < Test::Unit::TestCase
  include Utils

  def setup
    go_instructions = TestExplorer.new.process_robot_instructions(["1 1 E\n", "RFRFRFRF\n"]).first
    @planet = Planet.new([5, 3], 'Mars')

    @robot = Robot.new(go_instructions, @planet)
  end

  def teardown
    @robot = nil
    @planet = nil
  end

  def test_robot
    @robot.go
  end

  # TODO: add test for each rotation
  def test_rotate_left
    @robot.rotate_left
    assert(@robot.current_orientation == 'N')
    @robot.rotate_left
    assert(@robot.current_orientation == 'W')    
    @robot.rotate_left
    assert(@robot.current_orientation == 'S')    
    @robot.rotate_left
    assert(@robot.current_orientation == 'E')
  end

  # TODO: add test for each rotation
  def test_rotate_right
    @robot.rotate_right
    assert(@robot.current_orientation == 'S')
    @robot.rotate_right
    assert(@robot.current_orientation == 'W')
    @robot.rotate_right
    assert(@robot.current_orientation == 'N')
    @robot.rotate_right
    assert(@robot.current_orientation == 'E')
  end

  def test_robot_finish_alive
    robot = 
    @robot.go
    assert(@robot.to_s == '1 1 E')
  end

  def test_robot_died
    go_instructions = TestExplorer.new.process_robot_instructions(["3 2 N\n", "FRRFLLFFRRFLL\n"]).first 
    robot = Robot.new(go_instructions, @planet)
    robot.go
    assert(robot.to_s == "3 3 N LOST")
  end

  def test_robot_rescued
    @planet.set_rescue_object_on([3, 4])
    go_instructions = TestExplorer.new.process_robot_instructions(["0 3 W\n", "LLFFFLFLFL\n"]).first 
    robot = Robot.new(go_instructions, @planet)
    robot.go
    assert(robot.to_s == "2 3 S")
  end
end