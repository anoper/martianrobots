require 'ostruct'
require_relative 'planet'
require_relative 'robot'

class Explorer
  MAX_COORDINATE = 50
  MAX_INSTRUCTIONS_LENGTH = 100
  DEFAULT_PLANET = 'Mars'

  def initialize(instruction_register_path, planet_name)
    file = load_instruction_file(instruction_register_path)

    all_instructions = process_instructions(file)
    planet_boundaries = all_instructions[:planet_boundaries]
    go_instructions = all_instructions[:go_instructions]

    explore(planet_boundaries, go_instructions)
  end

  def explore(planet_boundaries, go_instructions, planet_name=nil)
    planet_name = planet_name.nil? ? DEFAULT_PLANET : planet_name
    @planet = Planet.new(planet_boundaries, planet_name)

    go_instructions.each do |instructions|
      robot = Robot.new(instructions, @planet)
      robot.go
      robot.print_report_state
    end
  end

  def process_instructions(raw_instructions)
    instructions = {}
    
    planet_bounderies = process_planet_boundaries(raw_instructions[0])
    go_instructions = process_robot_instructions(raw_instructions[1..raw_instructions.length])
    
    instructions[:planet_boundaries] = planet_bounderies
    instructions[:go_instructions] = go_instructions

    instructions
  end

  # raw_instructions - consists of start position and instructions for steps
  def process_robot_instructions raw_instructions
    go_instructions = []

    raw_instructions.each_slice(2) do |instruct_batch|
      go_instructions << process_go_instruction(instruct_batch)
    end

    go_instructions
  end

  def process_go_instruction(raw_instructions)
    start_instruction = parse_start_instruction(raw_instructions[0])
    go_instructions = parse_go_navigations(raw_instructions[1])

    { start_instruction: start_instruction, instructions: go_instructions }
  end

  def parse_go_navigations raw_instructions
    raise ArgumentError, "Invalid instruction set #{raw_instructions}" unless valid_instructions?(raw_instructions)
    raise ArgumentError, "Instructions set too long. Maximum size is #{MAX_INSTRUCTIONS_LENGTH}, your is #{raw_instructions.length}"  if raw_instructions.length > MAX_INSTRUCTIONS_LENGTH
    instructions = raw_instructions.strip.upcase
  end

  def parse_start_instruction raw_instructions
    x, y, start_orientation = raw_instructions.split(' ')
    coordinates = parse_coordinates([x, y])
    raise ArgumentError, "Invalid orientation #{raw_instructions}" unless valid_orientation?(start_orientation)
    
    start_instruction = OpenStruct.new(:coordinates => coordinates, :orientation => start_orientation)
  end

  def parse_coordinates raw_coordinates
    raise ArgumentError, "Invalid coordinates, their format must be [x, y] they are #{raw_coordinates}" unless raw_coordinates.size == 2
    coordinates = raw_coordinates.map{ |c| c.scan(/\d+/).join(' ') }
    raise ArgumentError, 'Coordinates must be integer numbers' if coordinates.first.size.zero? || coordinates.last.size.zero?

    coordinates.map!(&:to_i)
    
    if (coordinates.first > MAX_COORDINATE) || (coordinates.last > MAX_COORDINATE)
      raise ArgumentError, "Coordinate value not allowed. Maximum coordinate allowance is #{MAX_COORDINATE} your coordinates are #{[coordinates.first, coordinates.last]}"
    end

    coordinates
  end

  private
  def process_planet_boundaries raw_instructions
    coordinates = raw_instructions.split(' ')
    parse_coordinates(coordinates)
  end

  def load_instruction_file(path)
    raw_instructions = File.open(path).read.lines
  end

  def valid_orientation? raw_orientation
    orientation.include?(raw_orientation)
  end

  def valid_instructions? raw_instructions
    instruction_set = raw_instructions.strip.split("")

    instruction_set.each do |i|
      return false unless instructions.include?(i)
    end

    true
  end

  def orientation
    %(N S E W)
  end

  def instructions
    %(L R F)
  end
end