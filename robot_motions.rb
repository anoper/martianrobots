module RobotMotions
  def rotate_left
    current_orientation_index = orientations.index(@current_orientation)
    if current_orientation_index == (orientations.size - 1)
      @current_orientation = orientations[0]
    else
      @current_orientation = orientations[current_orientation_index + 1]
    end
  end

  def rotate_right
    current_orientation_index = orientations.index(@current_orientation)
    if current_orientation_index == 0
      @current_orientation = orientations[orientations.count-1]
    else
      @current_orientation = orientations[current_orientation_index - 1]
    end
  end

  def step
    new_position = step_vertical(@current_orientation, @current_position) if @current_orientation == 'N' || @current_orientation == 'S'
    new_position = step_horizontal(@current_orientation, @current_position) if @current_orientation == 'W' || @current_orientation == 'E'  

    save_position(new_position)
  end


  def step_vertical orientation, position
    case orientation
    when 'N' then return [position.first, (position.last+1)]
    when 'S' then return [position.first, (position.last-1)]
    end
  end

  def step_horizontal  orientation, position
    case orientation
    when 'E' then return [position.first+1, position.last]
    when 'W' then return [position.first-1, position.last]
    end
  end

  def save_position(position)
    if @planet.on_planet?(position.first, position.last)
      @current_position = position
      true
    else
      unless @planet.rescued_object?(position)
        @planet.set_rescue_object_on(position)
        die
      end
      false
    end
  end

  def orientations
    %w(N W S E)
  end
end